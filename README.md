# 3088 PiHat Project

The PiHat in this case is used as a UPS (Uninterruptible Power Supply). This allows for users to be able to save their work etc. during times of loadshedding for example.

The PiHat will require a number of different components, including:
-OPAMPS
-9.6V Battery
- BJTS
- LEDs
- Resistors
- Diodes

These componets will be used across 3 subsystems namely:
- Voltage Supply and Battery Charging Circuit
- Current Shunt for monitoring the voltage
- LED status indictors
